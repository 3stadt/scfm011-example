# Beispielprogramm für shopcast.fm

Hier ein kleines Beispiel wie über Go und Otto in Javascript eine API bereitgestellt wird.

https://shopcast.fm/011

Die `main.go` Datei ist das eigentliche Programm. Sie lädt eine Javascript-Datei und interpretiert sie, dabei kann nun im Javascript die Funktion `scfmSum` aufgerufen werden, die Go-Code ausführt. Eine Mini-Programm Api. :-)
