package main

import (
	"fmt"
	"github.com/robertkrimen/otto"
	"io/ioutil"
	"os"
)

// This function can be used from javascript
func sum(a, b int64) int64 {
	return a + b
}

func main() {
	if len(os.Args) < 2 { // check for a file name
		fmt.Println("no javascript file name provided")
		os.Exit(1)
	}
	runOtto(os.Args[1]) // run the given javascript file in otto
}

func runOtto(file string) {

	// read the javascript from file
	js, err := ioutil.ReadFile(file)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	vm := otto.New()

	// Expose the scfmSum function to javascript
	vm.Set("scfmSum", func(call otto.FunctionCall) otto.Value {
		a, _ := call.Argument(0).ToInteger()
		b, _ := call.Argument(1).ToInteger()
		result, _ := vm.ToValue(sum(a, b))
		return result
	})

	// Run the javascript that was loaded above
	vm.Run(string(js))
}
